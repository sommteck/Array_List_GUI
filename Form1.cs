using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;   // Namespace für die Realisierung der ArrayList

namespace Array_List_GUI
{
    public partial class moeglichkeiten_einer_arraylist : Form
    {
        ArrayList liste = new ArrayList();

        public moeglichkeiten_einer_arraylist()
        {
            InitializeComponent();
            // Listarray füllen
            liste.Add(13);
            liste.Add("Hello world");
            liste.Add(11.24);
            liste.Add('X');
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
        }

        // Komplette ArrayList anzeigen
        private void cmd_arraylist_view_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();

            foreach (object elemente in liste)
                dataGridView1.Rows.Add(elemente);
        }

        // Es wird nur ein zufälliges Element aus der ArrayList angezeigt
        private void cmd_arrayelement_anzeigen_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();

            Random zufall = new Random();
            int zz = zufall.Next(0, 4);
            dataGridView1.Rows.Add(liste[zz]);
        }

        // Es wird an der dritten Stelle das Element "eingefügtes Element" eingefügt
        private void cmd_arrayelement_einfuegen_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();

            liste.Insert(3, "eingefügtes Element");
            foreach (object elemente in liste)
                dataGridView1.Rows.Add(elemente);
        }

        // Entfernen des Elements "Hello world"
        private void cmd_arrayelement_entfernen_Click(object sender, EventArgs e)
        {
            liste.Remove("Hello world");
            foreach (object elemente in liste)
                dataGridView1.Rows.Add(elemente);
        }
    }
}