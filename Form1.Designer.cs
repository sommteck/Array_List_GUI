namespace Array_List_GUI
{
    partial class moeglichkeiten_einer_arraylist
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cmd_arraylist_view = new System.Windows.Forms.Button();
            this.cmd_arrayelement_anzeigen = new System.Windows.Forms.Button();
            this.cmd_arrayelement_einfuegen = new System.Windows.Forms.Button();
            this.cmd_arrayelement_entfernen = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.cmd_end = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.LN_Ausgabe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // cmd_arraylist_view
            // 
            this.cmd_arraylist_view.Location = new System.Drawing.Point(32, 34);
            this.cmd_arraylist_view.Name = "cmd_arraylist_view";
            this.cmd_arraylist_view.Size = new System.Drawing.Size(164, 39);
            this.cmd_arraylist_view.TabIndex = 0;
            this.cmd_arraylist_view.Text = "&ArrayList anzeigen";
            this.cmd_arraylist_view.UseVisualStyleBackColor = true;
            this.cmd_arraylist_view.Click += new System.EventHandler(this.cmd_arraylist_view_Click);
            // 
            // cmd_arrayelement_anzeigen
            // 
            this.cmd_arrayelement_anzeigen.Location = new System.Drawing.Point(32, 101);
            this.cmd_arrayelement_anzeigen.Name = "cmd_arrayelement_anzeigen";
            this.cmd_arrayelement_anzeigen.Size = new System.Drawing.Size(164, 45);
            this.cmd_arrayelement_anzeigen.TabIndex = 1;
            this.cmd_arrayelement_anzeigen.Text = "ein Arrayelement &anzeigen";
            this.cmd_arrayelement_anzeigen.UseVisualStyleBackColor = true;
            this.cmd_arrayelement_anzeigen.Click += new System.EventHandler(this.cmd_arrayelement_anzeigen_Click);
            // 
            // cmd_arrayelement_einfuegen
            // 
            this.cmd_arrayelement_einfuegen.Location = new System.Drawing.Point(32, 177);
            this.cmd_arrayelement_einfuegen.Name = "cmd_arrayelement_einfuegen";
            this.cmd_arrayelement_einfuegen.Size = new System.Drawing.Size(164, 36);
            this.cmd_arrayelement_einfuegen.TabIndex = 2;
            this.cmd_arrayelement_einfuegen.Text = "Arrayelement ein&fügen";
            this.cmd_arrayelement_einfuegen.UseVisualStyleBackColor = true;
            this.cmd_arrayelement_einfuegen.Click += new System.EventHandler(this.cmd_arrayelement_einfuegen_Click);
            // 
            // cmd_arrayelement_entfernen
            // 
            this.cmd_arrayelement_entfernen.Location = new System.Drawing.Point(32, 243);
            this.cmd_arrayelement_entfernen.Name = "cmd_arrayelement_entfernen";
            this.cmd_arrayelement_entfernen.Size = new System.Drawing.Size(164, 33);
            this.cmd_arrayelement_entfernen.TabIndex = 3;
            this.cmd_arrayelement_entfernen.Text = "Arrayelemen&t entfernen";
            this.cmd_arrayelement_entfernen.UseVisualStyleBackColor = true;
            this.cmd_arrayelement_entfernen.Click += new System.EventHandler(this.cmd_arrayelement_entfernen_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.Location = new System.Drawing.Point(260, 368);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(102, 46);
            this.cmd_clear.TabIndex = 4;
            this.cmd_clear.Text = "&DataGrid Löschen";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(423, 368);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(86, 46);
            this.cmd_end.TabIndex = 5;
            this.cmd_end.Text = "&Ende";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LN_Ausgabe});
            this.dataGridView1.Location = new System.Drawing.Point(260, 34);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(249, 294);
            this.dataGridView1.TabIndex = 6;
            // 
            // LN_Ausgabe
            // 
            this.LN_Ausgabe.HeaderText = "Ausgabe";
            this.LN_Ausgabe.Name = "LN_Ausgabe";
            this.LN_Ausgabe.Width = 200;
            // 
            // moeglichkeiten_einer_arraylist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 442);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_arrayelement_entfernen);
            this.Controls.Add(this.cmd_arrayelement_einfuegen);
            this.Controls.Add(this.cmd_arrayelement_anzeigen);
            this.Controls.Add(this.cmd_arraylist_view);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "moeglichkeiten_einer_arraylist";
            this.Text = "Möglichkeiten einer ArrayList";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cmd_arraylist_view;
        private System.Windows.Forms.Button cmd_arrayelement_anzeigen;
        private System.Windows.Forms.Button cmd_arrayelement_einfuegen;
        private System.Windows.Forms.Button cmd_arrayelement_entfernen;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn LN_Ausgabe;
    }
}

